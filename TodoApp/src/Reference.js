import * as firebase from 'firebase';

const config = {
    apiKey: "AIzaSyARqcJM6VfJbtQjGb_p4rwNdpZbfSQYOEI",
    authDomain: "rn-todo-9091a.firebaseapp.com",
    databaseURL: "https://rn-todo-9091a-default-rtdb.firebaseio.com",
    projectId: "rn-todo-9091a",
    storageBucket: "rn-todo-9091a.appspot.com",
    messagingSenderId: "501639662891",
    appId: "1:501639662891:web:274b47d968b1c4a763096e",
    measurementId: "G-66QH0PE7PV"
};
firebase.initializeApp(config);

const rootRef = firebase.database().ref();
export const tasksRef = rootRef.child('tasks');
export const timeRef = firebase.database.ServerValue.TIMESTAMP;

