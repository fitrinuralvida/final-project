import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { Container, Header, Content, FactoryComponentProps, FooterTab, Button, Icon, Form, ITheme, Input, Label, TextArea, 
         Left, Body, Title, Right, Row, Tabs, Tab } from 'native-base'

export default class Navbar extends Component {
    render(){
        return(
            <Header noShadow>
                <Left>
                    <Button transparent>
                        <Icon name="arrow-back"/>
                    </Button>
                </Left>
                <Body>
                    <Title style={{marginLeft:40}}>My Todo</Title>
                </Body>
                <Right>
                    <Button transparent>
                        <Icon name="menu"/>
                    </Button>
                </Right>
            </Header>
        );
    }
}